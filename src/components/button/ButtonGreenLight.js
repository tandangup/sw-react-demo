import React from 'react';
import classNames from 'classnames';

function ButtonGreenLight({ content, addClass, ...rest }) {
  return (
    <button
      {...rest}
      className={classNames(
        'rounded-md text-white px-4 py-2 text-green-600 border border-green-600 hover:bg-green-100 disabled:opacity-50',
        {
          [addClass]: addClass,
        }
      )}
    >
      {content}
    </button>
  );
}

ButtonGreenLight.defaultProps = {
  addClass: null,
};

export default ButtonGreenLight;
