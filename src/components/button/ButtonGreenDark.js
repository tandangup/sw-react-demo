import React from 'react';
import classNames from 'classnames';

function ButtonGreenDark({ content, addClass, ...rest }) {
  return (
    <button
      {...rest}
      className={classNames(
        'rounded-md text-white px-4 py-2 text-white border bg-green-600 border-green-600 hover:bg-green-700 disabled:opacity-50',
        {
          [addClass]: addClass,
        }
      )}
    >
      {content}
    </button>
  );
}

ButtonGreenDark.defaultProps = {
  addClass: null,
};

export default ButtonGreenDark;
