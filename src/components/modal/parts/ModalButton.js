export function PrimaryModalButton(props) {
  return (
    <li>
      <a href="#!">{props.children}</a>
    </li>
  );
}

export function SecondaryModalButton(props) {
  return (
    <li className="secondary-modal-button">
      <a href="#!">{props.children}</a>
    </li>
  );
}
