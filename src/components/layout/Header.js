import React, { useRef } from 'react';
// import Logo2 from '@images/logo/logo.png';
import { Menu, Dropdown, Select } from 'antd';
import * as actions from '../../store/actions';
import { useMediaQuery } from 'react-responsive';
// import Logo from '@images/logo/igooana_logo.png';
import { Link, withRouter } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
// import Avatar from '@images/portrait/small/avatar.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import tabsquarelogo from '@logo/logo-2019.svg';
import Searchicon from '@icon/search.svg';
import Burgericon from '@icon/burger.svg';

const { Option } = Select;

function Header({ history }) {
  const searchRef = useRef();

  const dispatch = useDispatch();

  // const mediumScreen = useMediaQuery({
  //   query: '(min-width: 768px)',
  // });

  const user = useSelector((state) => state.auth.user);

  const role = useSelector((state) => state.auth.user.userRole);

  const handleLogout = () => {
    dispatch(actions.logout());
    history.push('/login');
  };

  const handleSearch = (data) => {
    searchRef.current.blur();
    history.push(data);
  };

  const menu = (
    <Menu>
      <Menu.Item key="0">
        <Link to="/profile" className="text-sm text-gray-700">
          <div className="flex justify-between items-center">
            <span>Profile</span>
            <FontAwesomeIcon icon={faUser} color="gray" />
          </div>
        </Link>
      </Menu.Item>
      <Menu.Item key="2" onClick={handleLogout}>
        <div className="flex justify-between items-center">
          <button className="text-sm text-gray-700">Sign out</button>
          <FontAwesomeIcon icon={faSignOutAlt} color="gray" />
        </div>
      </Menu.Item>
    </Menu>
  );

  return (
    <div className="grid grid-cols-12 w-full content-center justify-center h-14">
      <div className="col-span-3 justify-start flex px-3">
        <img
          className="w-7 h-7 "
          src={Burgericon}
          alt="Merchant Logo"
          width={30}
          height={30}
        />
      </div>
      <div className="col-span-6 flex">
        <img
          className="w-auto h-8"
          src={tabsquarelogo}
          alt="Merchant Logo"
          width={120}
          height={30}
        />
      </div>
      <div className="col-span-3 justify-end flex px-3">
        <img
          className="w-7 h-7"
          src={Searchicon}
          alt="Merchant Logo"
          width={30}
          height={30}
        />
      </div>
    </div>
  );
}

export default withRouter(Header);
