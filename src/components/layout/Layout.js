import React from 'react';
import { withRouter } from 'react-router-dom';

function Layout({ children }) {
  return (
    <div className="min-h-screen flex flex-col flex-auto flex-shrink-0 antialiased bg-white text-black">
      <div id="content-area" className="">
        {children}
      </div>
    </div>
  );
}

export default withRouter(Layout);
