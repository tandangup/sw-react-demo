import React from 'react';

function Footer() {
  return (
    <footer className="p-4 text-xs md:text-base text-gray-600 ">
      COPYRIGHT @ 2021 Smartweb, All rights Reserved
    </footer>
  );
}

export default Footer;
