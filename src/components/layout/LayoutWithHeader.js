import Header from './Header';
import Footer from './Footer';
import Sidebar from './Sidebar';
import React from 'react';
import { withRouter } from 'react-router-dom';

function LayoutWithHeader({ children, header }) {
  return (
    <div className="min-h-screen with header flex flex-col flex-auto flex-shrink-0 antialiased bg-white text-black">
      <Header />
      {/* <Sidebar /> */}
      <div id="content-area" className="">
        {children}
        <Footer />
      </div>
    </div>
  );
}

export default withRouter(LayoutWithHeader);
