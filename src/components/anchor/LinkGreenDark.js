import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

function LinkGreenDark({ content, addClass, ...rest }) {
  return (
    <Link
      {...rest}
      className={classNames(
        'rounded-md text-white px-4 py-2 text-white border bg-green-600 border-green-600 hover:bg-green-700',
        {
          [addClass]: addClass,
        }
      )}
    >
      {content}
    </Link>
  );
}

LinkGreenDark.defaultProps = {
  addClass: null,
};

export default LinkGreenDark;
