import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

function LinkGreenLight({ content, addClass, ...rest }) {
  return (
    <Link
      {...rest}
      className={classNames(
        'rounded-md text-white px-4 py-2 text-green-600 border border-green-600 hover:bg-green-100',
        {
          [addClass]: addClass,
        }
      )}
    >
      {content}
    </Link>
  );
}

LinkGreenLight.defaultProps = {
  addClass: null,
};

export default LinkGreenLight;
