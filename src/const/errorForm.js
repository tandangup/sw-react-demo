const required = 'This field is required';
const validEmail = 'Pleas insert valid email';
const passwordNotMatch = 'Password confirmation not match';

const errorForm = {
  required: required,
  validEmail: validEmail,
  passwordNotMatch: passwordNotMatch,
};

export default errorForm;
