import authReducer from './store/reducers/authReducer';
// import projectReducer from './store/reducers/projectReducer';

import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware, combineReducers } from 'redux';

const rootReducer = combineReducers({
  auth: authReducer,
  // project: projectReducer,
});

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

export default store;
