const listRole = {
  public: [
    'admin',
    'branch',
    'company',
    'project',
    'new_user',
    'employee',
    'hr_admin',
    'new_employee',
    'project_viewer',
  ],
  branch: [
    'admin',
    'branch',
    'company',
    'project',
    'hr_admin',
    'project_viewer',
  ],
  admin: ['admin'],
  new_user: ['new_user'],
  employee: ['employee'],
  company: ['company', 'admin'],
  project: ['project', 'admin'],
  project_viewer: ['project_viewer'],
  new_employee: ['new_employee', 'employee'],
  regional: ['regional', 'admin', 'company', 'project'],
  hr_admin: ['hr_admin', 'admin', 'project', 'project_viewer', 'company'],
};

export default listRole;
