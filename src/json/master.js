export const sizeShoes = [35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46];

export const sizeClothing = ['S', 'M', 'L', 'XL', 'XXL', 'XXXL'];

export const drivingLicense = ['A', 'C', 'AB'];

export const golDarah = ['A', 'AB', 'B', '0'];

export const maritalOptions = [
  { label: 'Single', value: 'single' },
  { label: 'Married', value: 'married' },
  { label: 'Widowed', value: 'widowed' },
  { label: 'Divorced', value: 'divorced' },
];

export const religionOptions = [
  { label: 'Islam', value: 'islam' },
  { label: 'Kristen', value: 'kristen' },
  { label: 'Katolik', value: 'katolik' },
  { label: 'Hindu', value: 'hindu' },
  { label: 'Budha', value: 'budha' },
  { label: 'Konghucu', value: 'konghucu' },
];

export const educationLevels = [
  { label: 'No School', value: 'noschool' },
  { label: 'Elementary School', value: 'elementary' },
  { label: 'Junior High School', value: 'junior' },
  { label: 'Senior High School', value: 'senior' },
  { label: 'D1 College', value: 'd1' },
  { label: 'D3 College', value: 'd3' },
  { label: 'S1 College', value: 's1' },
  { label: 'S2 College', value: 's2' },
];

export const pajakMethods = [
  { label: 'Nett', value: 'net' },
  { label: 'Gross', value: 'gross' },
  { label: 'Gross Up', value: 'gross_up' },
];

export const ptkpStatus = [
  { label: 'TK0', value: 'tk0' },
  { label: 'TK1', value: 'tk1' },
  { label: 'TK2', value: 'tk2' },
  { label: 'K0', value: 'k0' },
  { label: 'K1', value: 'k1' },
  { label: 'k2', value: 'K2' },
];

export const bankOptions = [
  { label: 'Bank Mandiri', value: 'mandiri' },
  { label: 'Bank BCA', value: 'bca' },
  { label: 'Bank BRI', value: 'bri' },
  { label: 'Bank BNI', value: 'bni' },
];

export const wargaOptions = [
  { label: 'Indonesia', value: 'indonesia' },
  { label: 'Asing', value: 'asing' },
];

export const relationsOptions = [
  { label: 'Orang Tua', value: 'ortu' },
  { label: 'Istri', value: 'istri' },
  { label: 'Suami', value: 'suami' },
  { label: 'Kakak Kandung', value: 'kakak' },
  { label: 'Adik Kandung', value: 'adik' },
  { label: 'Saudara', value: 'saudara' },
];

export const countryOptions = [{ label: 'Indonesia', value: 1 }];

export const languageSkillOption = [
  { label: 'Basic', value: 'basic' },
  { label: 'Intermediate', value: 'intermedia' },
  { label: 'Fluent', value: 'fluent' },
];

export const computerSkillOption = [
  { label: 'Basic', value: 'basic' },
  { label: 'Intermediate', value: 'intermedia' },
  { label: 'Advance', value: 'advance' },
];

export const degreeOptions = [
  { label: 'Bachelor', value: 'bacheclor' },
  { label: 'Master', value: 'master' },
];

export const incomeOptions = [
  { label: '< 5 Juta', value: '< 5 Juta' },
  { label: '5 - 10 Juta', value: '5 - 10 Juta' },
  { label: '11 - 20 Juta', value: '11 - 20 Juta' },
  { label: '> 20 Juta', value: '> 20 Juta' },
];

export const workStatusOptions = [
  { label: 'Freelance', value: 'freelance' },
  { label: 'Kontrak', value: 'kontrak' },
  { label: 'Permanen', value: 'permanen' },
  { label: 'Resign', value: 'resign' },
];
