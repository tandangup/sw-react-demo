export function register() {
  console.log('tester services');
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
      .register('/sw.js', { scope: '/' })
      .then((registration) => {
        console.log('Services worker registered');
      });
    navigator.serviceWorker.ready.then((registration) => {
      console.log('Services worker ready');
    });
  }
}
