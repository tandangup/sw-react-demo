import React, { lazy, Suspense } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
const Layout = lazy(() => import('./components/layout/Layout'));

const Login = lazy(() => import('./pages/Login'));
const Scan = lazy(() => import('./pages/Scan'));
const Intro = lazy(() => import('./pages/Intro'));
const Home = lazy(() => import('./pages/Home'));

const ScrollToTop = () => {
  window.scrollTo(0, 0);
  return null;
};

const RouteWithoutHeader = ({ component: Component, ...props }) => {
  return (
    <Route
      {...props}
      render={(routeProps) => (
        <Layout>
          <Component {...routeProps} />{' '}
        </Layout>
      )}
    />
  );
};

const renderLoader = () => <p>Loading</p>;

const Main = () => (
  <Suspense fallback={renderLoader}>
    <Route component={ScrollToTop} />
    <Switch>
      <RouteWithoutHeader path="/login" exact component={Login} />
      <RouteWithoutHeader path="/intro" exact component={Intro} />
      {/* Normal Route */}
      <Route exact path="/scan/:id" component={Scan} />
      <Route render={() => <Redirect to="/login" />} />
    </Switch>
  </Suspense>
);

export default Main;
