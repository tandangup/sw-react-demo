// import './antd.less';
// import React from 'react';
// import _ from 'lodash';
import Route from './router';
import { SWRConfig } from 'swr';
import fetcher from './helper/fetcher';
// import Layout from './components/layout/Layout';
// import { ToastContainer } from 'react-toastify';

function App({ location }) {
  console.log(location, 'location');
  /**
   * select a router without layout
   */
  // const noLayout = [
  //   '/login',
  //   '/intro',
  //   '/register',
  //   '/not-found',
  //   '/select-project',
  //   '/not-authorized',
  //   '/reset-password',
  //   '/forgot-password',
  // ];

  // const isAuth = useSelector((state) => state.auth.isAuth);

  // useEffect(() => {
  // const generateCSRF = async () => {
  //   await apiServices.getCSRF();
  // };
  // if (!isAuth) generateCSRF();
  // }, [isAuth]);

  // if (_.includes(noLayout, location.pathname)) {
  //   return children;
  // }

  return (
    <SWRConfig
      value={{
        fetcher: fetcher,
        revalidateOnMount: true,
        revalidateIfStale: false,
        revalidateOnFocus: false,
        revalidateOnReconnect: false,
      }}
    >
      <Route />
      {/* <ToastContainer
          draggable
          pauseOnHover
          closeOnClick
          pauseOnFocusLoss
          rtl={false}
          autoClose={2000}
          newestOnTop={false}
          position="top-center"
          hideProgressBar={false}
        /> */}
    </SWRConfig>
  );
}

export default App;
