import React, { useState, useEffect } from 'react';
import { Wrapper } from '../styled-components/auth';
import tabsquarelogo from '@images/logo/logo-2019.svg';
// import '@css/Intro.scss';

function Intro() {
  return (
    <Wrapper>
      <div className="h-screen content-center p-32 justify-center flex">
        <img src={tabsquarelogo} alt="intro logos" />
      </div>
    </Wrapper>
  );
}
export default Intro;
