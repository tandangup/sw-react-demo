import React, { useState } from 'react';
import apiServices from '../api-services';
import { useForm } from 'react-hook-form';
import errorForm from '../const/errorForm';
import { Wrapper } from '../styled-components/auth';
import { ErrorMessage } from '@hookform/error-message';
import ImgForgot from '@images/pages/forgot-password.png';
import LinkGreenLight from '../components/anchor/LinkGreenLight';
import ButtonGreenDark from '../components/button/ButtonGreenDark';

function ForgotPassword({ history }) {
  const {
    setError,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const [loading, setLoading] = useState(false);

  const handleForgot = (data) => {
    setLoading(true);
    apiServices
      .forgotPassword(data)
      .then(() => {
        setLoading(false);
        history.push({
          pathname: '/login',
          state: { message: 'Password reset email sent.' },
        });
        history.push('/login');
      })
      .catch((err) => {
        setLoading(false);
        setError('email', {
          type: 'manual',
          message: err.response.data.message,
        });
      });
  };

  return (
    <Wrapper className="min-h-screen flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
      <div className="sm:w-1/2 lg:w-3/5">
        <div className="flex items-center bg-gray-100">
          <div className="hidden lg:block lg:w-1/2">
            <img alt="Igooana" src={ImgForgot} className="mx-auto" />
          </div>
          <div className="w-full lg:w-1/2 bg-white p-8">
            <form onSubmit={handleSubmit(handleForgot)}>
              <div className="mb-4">
                <h3 className="mb-4">Recover your password</h3>
                <p>
                  Please enter your email address and we'll send you
                  instructions on how to reset your password.
                </p>
              </div>
              <div className="pb-4">
                <input
                  type="text"
                  placeholder="Email"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  {...register('email', {
                    required: errorForm.required,
                    pattern: {
                      value: /^\S+@\S+$/i,
                      message: errorForm.validEmail,
                    },
                  })}
                />
                <span className="text-sm text-red-600">
                  <ErrorMessage name="email" errors={errors} />
                </span>
              </div>
              <div className="flex items-center justify-between pb-4">
                <LinkGreenLight to="/login" content="Login" />
                <ButtonGreenDark
                  type="submit"
                  content="Submit"
                  disabled={loading}
                />
              </div>
            </form>
          </div>
        </div>
      </div>
    </Wrapper>
  );
}

export default ForgotPassword;
