import { Modal } from 'antd';
import classNames from 'classnames';
import React, { useEffect } from 'react';
import errForm from '../../const/errorForm';
import { Form, Row, Col, Input, Select } from 'antd';

const formItemLayout = {
  labelAlign: 'left',
  labelCol: {
    lg: { span: 24 },
    md: { span: 24 },
    xs: { span: 24 },
  },
};

const modalStyle = {
  height: '300px',
  overflow: 'auto',
};

const { Option } = Select;

function ModalActivity({
  data,
  title,
  visible,
  loading,
  onCreate,
  onUpdate,
  doAction,
  handleClose,
}) {
  const [form] = Form.useForm();

  useEffect(() => {
    visible && form.resetFields();
  }, [form, visible]);

  const handleCloseForm = () => {
    handleClose();
  };

  const handleSubmit = () => {
    form.validateFields().then((values) => {
      if (title === 'Add') onCreate(values);
      if (title === 'Edit') onUpdate(values);
    });
  };

  return (
    <Modal
      width={800}
      title={title}
      visible={visible}
      onOk={handleSubmit}
      bodyStyle={modalStyle}
      confirmLoading={loading}
      onCancel={handleCloseForm}
      okButtonProps={{
        disabled: !doAction,
      }}
      cancelButtonProps={{
        disabled: !doAction,
      }}
    >
      <Form
        form={form}
        size="large"
        name="activity_form"
        className={classNames({
          'pointer-events-none': !doAction,
        })}
        {...formItemLayout}
      >
        <Row gutter={32}>
          <Col span={12}>
            <Form.Item
              name="user_type"
              label="User Type"
              initialValue={data.user_type}
              rules={[
                {
                  required: true,
                  message: errForm.required,
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="sub_type"
              label="Sub Type"
              initialValue={data.sub_type}
              rules={[
                {
                  required: true,
                  message: errForm.required,
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="status"
              label="Status"
              initialValue={data.status}
              rules={[
                {
                  required: true,
                  message: errForm.required,
                },
              ]}
            >
              <Select>
                <Option value={1}>Active</Option>
                <Option value={0}>Non Active</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
}

ModalActivity.defaultProps = {
  data: {},
};

export default ModalActivity;
