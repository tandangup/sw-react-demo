import useSWR from 'swr';
import { toast } from 'react-toastify';
import listRole from '../../json/roles';
import React, { useState } from 'react';
import RBAC from '../../components/RBAC';
import { useSelector } from 'react-redux';
import APIS from '../../api-services/apis';
import ModalUserType from './ModalUserType';
import apiServices from '../../api-services';
import useDataTable from '../../hooks/useDataTable';
import { Table, Space, Input, Tag, Modal } from 'antd';
import ModalImport from '../../components/modal/ModalImport';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ButtonGreenDark from '../../components/button/ButtonGreenDark';
import ButtonGreenLight from '../../components/button/ButtonGreenLight';
import { faEdit, faTrashAlt, faEye } from '@fortawesome/free-regular-svg-icons';

const { Search } = Input;

const { confirm } = Modal;

function UserType() {
  const [selected, setSelected] = useState({});
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [doAction, setDoAction] = useState(false);
  const [modalTitle, setModalTitle] = useState(null);
  const [modalImport, setModalImport] = useState(false);

  const role = useSelector((state) => state.auth.user.userRole);

  const [
    page,
    total,
    search,
    perPage,
    setSearch,
    setPage,
    setTotal,
    setPerpage,
  ] = useDataTable();

  const {
    data: userType,
    error,
    mutate,
  } = useSWR(
    `${APIS.LIST_USER_TYPE}?page=${page}&search=${search}&limit=${perPage}`,
    {
      onSuccess: (value) => {
        if (value.total) setTotal(value.total);
      },
    }
  );

  const handleCloseModal = () => {
    setSelected({});
    setDoAction(false);
    setModalTitle(null);
    setVisible(false);
  };

  const handleShowModal = (data, footer, title) => {
    setSelected(data);
    setDoAction(footer);
    setModalTitle(title);
    setVisible(true);
  };

  const handleAddData = () => {
    setDoAction(true);
    setModalTitle('Add');
    setVisible(true);
  };

  const onCreate = async (data) => {
    setLoading(true);
    try {
      await apiServices.addUserType(data);
      await mutate();
      setVisible(false);
      setDoAction(false);
      setModalTitle(null);
      setLoading(false);
    } catch {
      setLoading(false);
      toast.error('Failed creating data');
    }
  };

  const onUpdate = async (data) => {
    setLoading(true);
    try {
      await apiServices.editUserType(selected.id, data);
      await mutate();
      setSelected({});
      setVisible(false);
      setDoAction(false);
      setModalTitle(null);
      setLoading(false);
    } catch {
      setLoading(false);
      toast.error('Failed updating data');
    }
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: 'Do you want to delete these items?',
      icon: <ExclamationCircleOutlined />,
      onOk() {
        return new Promise((resolve, reject) => {
          apiServices
            .deleteUserType(id)
            .then(() => {
              mutate().then(() => {
                resolve();
              });
            })
            .catch(() => {
              toast.error('Failed deleting data');
              reject();
            });
        });
      },
    });
  };

  const handleCancleImport = () => {
    setModalImport(false);
  };

  const handleShowImport = () => {
    setModalImport(true);
  };

  const handleSearch = (value) => {
    setSearch(value);
  };

  const handleExport = async () => {
    try {
      const json = await apiServices.exportProject();
      const result = await json.data;
      window.open(result.file);
    } catch (e) {
      toast.error('Export data failed');
    }
  };

  const handleImport = async (data) => {
    try {
      await apiServices.importProject(data);
      setModalImport(false);
      toast.success('Data has been successfully imported');
    } catch (e) {
      toast.error('Import data failed');
    }
  };

  const columns = [
    {
      title: 'Commercial Name',
      dataIndex: 'commercial_name',
      hidden: role !== 'admin',
    },
    {
      title: 'User Type',
      dataIndex: 'user_type',
    },
    {
      title: 'Sub User',
      dataIndex: 'sub_type',
    },
    {
      width: 100,
      title: 'Status',
      dataIndex: 'status',
      render: (value) => (
        <>
          {value === 1 && <Tag color="#87d068">Active</Tag>}
          {value === 0 && <Tag color="#DA3C24 ">Non Active</Tag>}
        </>
      ),
    },
    {
      width: 150,
      dataIndex: '',
      fixed: 'right',
      title: 'Action',
      align: 'center',
      render: (value) => (
        <Space size="large">
          <button
            className="text-gray-600"
            onClick={() => handleShowModal(value, false, 'View')}
          >
            <FontAwesomeIcon icon={faEye} />
          </button>
          <RBAC allowedRoles={[[listRole.project], [listRole.project_viewer]]}>
            <button
              className="text-gray-600"
              onClick={() => handleShowModal(value, true, 'Edit')}
            >
              <FontAwesomeIcon icon={faEdit} />
            </button>
          </RBAC>
          <RBAC allowedRoles={[[listRole.project], [listRole.project_viewer]]}>
            <button
              className="text-gray-600"
              onClick={() => showDeleteConfirm(value.id)}
            >
              <FontAwesomeIcon icon={faTrashAlt} />
            </button>
          </RBAC>
        </Space>
      ),
    },
  ].filter((item) => !item.hidden);

  return (
    <div className="content-wrapper">
      <div className="text-2xl font-bold mb-5">User Type Data</div>
      <div className="flex justify-between mb-5">
        <div className="flex flex-wrap gap-2">
          <RBAC
            allowedRoles={[
              [listRole.project],
              [listRole.admin, listRole.project_viewer],
            ]}
          >
            <ButtonGreenLight content="+ Add New" onClick={handleAddData} />
          </RBAC>
          <RBAC
            allowedRoles={[
              [listRole.project],
              [listRole.admin, listRole.project_viewer],
            ]}
          >
            <ButtonGreenLight
              content="Import Data"
              onClick={handleShowImport}
            />
          </RBAC>
          <ButtonGreenDark content="Export Data" onClick={handleExport} />
        </div>
        <Search
          placeholder="Search"
          style={{ width: 200 }}
          onSearch={handleSearch}
        />
      </div>
      <Table
        loading={!userType && !error}
        columns={columns}
        dataSource={
          userType && userType.data
            ? userType.data.map((item) => ({ ...item, key: item.id }))
            : []
        }
        scroll={{ x: 600, y: 450 }}
        pagination={{
          current: page,
          pageSize: perPage,
          showSizeChanger: true,
          onChange: (page, pageSize) => {
            setPage(page);
            setPerpage(pageSize);
          },
          total: total,
        }}
      />
      <ModalUserType
        data={selected}
        loading={loading}
        visible={visible}
        title={modalTitle}
        onCreate={onCreate}
        onUpdate={onUpdate}
        doAction={doAction}
        handleClose={handleCloseModal}
      />
      <ModalImport
        visible={modalImport}
        handleOk={handleImport}
        modalTitle="Import User Type"
        handleCancle={handleCancleImport}
      />
    </div>
  );
}

export default UserType;
