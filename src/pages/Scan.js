import { useParams, Redirect } from 'react-router-dom';
function Scan() {
  const { id } = useParams();

  if (id) {
    return <Redirect to="/login" />;
  } else {
    return <Redirect to="/not-found" />;
  }
}
export default Scan;
