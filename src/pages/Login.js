import { useForm } from 'react-hook-form';
import errorForm from '../const/errorForm';
import { Wrapper } from '../styled-components/auth';
import { useSelector } from 'react-redux';
import ButtonGreenDark from '../components/button/ButtonGreenDark';
import googlelogo from '@images/logo/google-16px.png';
import tabsquarelogo from '@images/logo/logo-2019.svg';
// import ModalAges from '../components/modal/ModalAges';

function Login({ location, history }) {
  // const dispatch = useDispatch();

  const {
    register,
    // handleSubmit,
    // formState: { errors },
  } = useForm();

  const loading = useSelector((state) => state.auth.loading);

  return (
    <Wrapper>
      <div className="drop-shadow-xl h-24 bg-white text-center content-center grid">
        <div>
          <h6 className="text-xs text-gray-500"> Hujan emas road </h6>
        </div>
      </div>
      <div className="w-full h-auto">
        <img
          alt="Merchant logos"
          className="w-full"
          width={100}
          height={100}
          src="https://storage.googleapis.com/skipque3/settings/smartweb_login_screen_1609299915_ikea-logo-new-sq-1.jpg"
        />
      </div>
      <div className="h-auto min-h-fit bg-gray-200 flex justify-center py-12 sm:px-6 lg:px-8">
        <div className="lg:w-4/5 w-3/4">
          {/* social media login buttons */}
          <div className="mb-8 flex md:flex-nowrap flex-wrap md:space-x-2 md:space-y-0 space-y-2">
            <button className="md:px-5 py-2 bg-red-500 w-full md:w-1/2 rounded-md text-white shadow-xl">
              <img
                src={googlelogo}
                alt="google logo"
                className="w-8 inline-flex pr-2"
                width={100}
                height={100}
              />
              Sign in with <b> Google </b>
            </button>
            <button className="md:px-5 py-2 bg-red-500 w-full md:w-1/2 rounded-md text-white shadow-xl">
              <img
                src={googlelogo}
                alt="google logo"
                className="w-8 inline-flex pr-2"
                width={100}
                height={100}
              />
              Sign in with <b> Google </b>
            </button>
          </div>
          {/* divider */}
          <div className="relative flex justify-center min-h-fit h-14 mb-6">
            <div className="bg-gray-200 z-10 p-4 absolute self-center uppercase text-lg text-gray-400">
              or sign in with
            </div>
            <div className="bg-gray-300 z-0 w-full h-0.5 absolute self-center"></div>
          </div>
          {/* login form tabs */}
          <div>
            <div className="grid grid-cols-2 mb-4 text-lg">
              <div className="border-b-4 border-red-500	text-center">
                <span className="text-red-500	font-bold"> Phone </span>
              </div>
              <div className="border-b-2 border-gray-400	text-center">
                <span className="text-gray-400	font-bold"> Email </span>
              </div>
            </div>
          </div>
          {/* login form inputs */}
          <div className="flex items-center">
            <div className="w-full lg:w-1/2 mt-3">
              <form>
                <div className="flex ">
                  <div className="pb-4 w-3/12">
                    <select className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                      <option> +62 </option>
                    </select>
                  </div>
                  <div className="pb-4 w-9/12">
                    <input
                      type="number"
                      placeholder="Phone Number"
                      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      {...register('password', {
                        required: errorForm.required,
                      })}
                    />
                    {/* <span className="text-sm text-red-600">
                      <ErrorMessage name="password" errors={errors} />
                    </span> */}
                  </div>
                </div>
                <div className="flex items-center justify-between pb-4">
                  <ButtonGreenDark
                    addClass="w-full shadow-lg"
                    type="submit"
                    content="Let's Go"
                    disabled={loading}
                  />
                </div>
              </form>
            </div>
          </div>
          {/* Privacy sections */}
          <div className="flex text-center text-xs mb-6">
            <h6 className="w-full">
              By clicking "<strong>LET’S GO</strong>", I agree to{' '}
              <a href="#!" data-openpop="title-normal">
                TabSquare Privacy Policy.
              </a>
            </h6>
          </div>
          <div className="flex flex-wrap text-center">
            <div className="w-full">
              <input type="checkbox" className="mr-1" />
              <span className="text-xs">
                I do not wish to receive any marketing or promotional materials.
              </span>
            </div>
            <div className="w-full">
              <input type="checkbox" className="mr-1" />
              <span className="text-xs">
                I agree to signup for the free membership program. ( For Active
                Mobile Numbers only )
              </span>
            </div>
          </div>
          {/* Divider sections */}
          <div className="bg-gray-300 h-0.5 w-full my-5"></div>
          {/* login as guess button */}
          <div className="flex text-center">
            <h6 className="w-full">
              <a href="#!" className="uppercase text-red-500 font-bold">
                Continue As Guess
              </a>
            </h6>
          </div>
          {/* copyright section */}
          <div className="flex text-center mt-4">
            <h6 className="w-full">
              <span className="inline-flex pr-2 font-thin"> Smartweb By </span>
              <img
                width={130}
                height={30}
                alt="Smartweb"
                src={tabsquarelogo}
                className="w-auto h-4 inline-flex"
              />
            </h6>
          </div>
        </div>
      </div>
      {/* <ModalAges /> */}
    </Wrapper>
  );
}

export default Login;
