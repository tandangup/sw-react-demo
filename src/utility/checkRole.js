import _ from 'lodash';

const getUserRole = (isAuth, user) => {
  if (isAuth && !_.isEmpty(user)) {
    const { userRole } = user;
    return userRole ? userRole : null;
  }
  return null;
};

const checkAccess = (role, listRoles) => {
  const check = listRoles.includes(role);
  return check;
};

export { getUserRole, checkAccess };
