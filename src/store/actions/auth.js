import { toast } from 'react-toastify';
import * as actionTypes from './actionTypes';
import apiServices from '../../api-services';

export const loginStart = () => {
  return {
    type: actionTypes.LOGIN_START,
  };
};

export const loginSuccess = (data) => {
  localStorage.setItem('user', JSON.stringify(data));
  return {
    type: actionTypes.LOGIN_SUCCESS,
    user: data,
  };
};

export const loginFail = () => {
  return {
    type: actionTypes.LOGIN_FAIL,
  };
};

export const login = (body, history) => {
  return async (dispatch) => {
    dispatch(loginStart());
    try {
      const login = await apiServices.login(body);
      const { user } = login.data;
      const { roles, ...rest } = user;
      const userData = {
        ...rest,
        role: roles.name,
        userRole: roles.slug,
      };
      dispatch(loginSuccess(userData));
      toast.success('Success Login');
      let redirectTo = '/';
      if (roles.slug === 'new_user') {
        redirectTo = '/company-data';
      } else if (roles.slug === 'new_employee') {
        redirectTo = '/employee-data';
      } else if (
        userData.userRole === 'project' ||
        userData.userRole === 'project_viewer'
      ) {
        redirectTo = '/select-project';
      }
      history.push(redirectTo);
    } catch (error) {
      dispatch(loginFail());
      toast.error(error.response.data.error);
    }
  };
};

export const logout = () => {
  localStorage.clear();
  return {
    type: actionTypes.USER_LOGOUT,
  };
};
