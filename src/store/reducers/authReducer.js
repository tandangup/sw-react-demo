import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../utility/updateObject';

const userData = localStorage.getItem('user');

const initialState = {
  error: false,
  loading: false,
  isAuth: userData ? true : false,
  user: userData ? JSON.parse(userData) : {},
};

const loginSuccess = (state, action) => {
  return updateObject(state, {
    user: action.user,
    isAuth: true,
    error: false,
    loading: false,
  });
};

const loginStart = (state, action) => {
  return updateObject(state, {
    loading: true,
  });
};

const loginFail = (state, action) => {
  return updateObject(state, {
    error: true,
    loading: false,
  });
};

const logout = (state, action) => {
  return updateObject(state, {
    user: {},
    isAuth: false,
  });
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOGIN_SUCCESS:
      return loginSuccess(state, action);
    case actionTypes.USER_LOGOUT:
      return logout(state, action);
    case actionTypes.LOGIN_FAIL:
      return loginFail(state, action);
    case actionTypes.LOGIN_START:
      return loginStart(state, action);
    default:
      return state;
  }
};

export default authReducer;
