import APIS from './apis';
import axiosApi from '../helper/axios';

const login = async (body) => {
  const json = await axiosApi({
    method: 'post',
    url: APIS.LOGIN,
    data: body,
  });
  const result = await json.data;
  return result;
};
// const findAllPromo = (id, orderType, userId, req) => {
//   const json = axiosApi({
//     method: 'get',
//     url: `smartweb/api/v4/categories/subcategories/dishes/?dishIds=${id.toString()}&orderTypes=${orderType}&userId=${userId}&skip_category=1${setupUrl(
//       req
//     )
//   })
//   return new Client(req.session.token, REQUEST_TIMEOUT).get(
//     }`
//   );
// };
// const getJwtKey = async () => {
//   const repsonse = await axiosApi({
//     metho
//   })
// }
const apiServices = {
  login: login,
};

export default apiServices;
