//PREFIX
const V1 = '/api/v1';

//AUTH
const API_LOGIN = '/login';

//DATA LIST
const API_LIST_CITY = `/api/city/by-province`;
const API_LIST_HOBBY = `${V1}/ajax/hobby`;
const API_EST_USER = `/api/estimated-user/all`;
const API_LIST_SKILLS = `${V1}/ajax/skill`;
const API_LIST_ETHNIC = `${V1}/ajax/ethnic`;
const API_LIST_VILLAGE = `${V1}/ajax/village`;
const API_LIST_ALL_CITY = `${V1}/ajax/allcity`;
const API_LIST_DISTRICT = `${V1}/ajax/district`;
const API_LIST_PROVINCE = `/api/province/all`;
const API_COMPANY_CATEGORY = `/api/category/all`;

const APIS = {
  LOGIN: API_LOGIN,
  LIST_CITY: API_LIST_CITY,
  LIST_HOBBY: API_LIST_HOBBY,
  EST_USER: API_EST_USER,
  LIST_SKILLS: API_LIST_SKILLS,
  LIST_ETHNIC: API_LIST_ETHNIC,
  LIST_VILLAGE: API_LIST_VILLAGE,
  LIST_ALL_CITY: API_LIST_ALL_CITY,
  LIST_DISTRICT: API_LIST_DISTRICT,
  LIST_PROVINCE: API_LIST_PROVINCE,
  COMPANY_CATEGORY: API_COMPANY_CATEGORY,
};

export default APIS;
