import { useState, useEffect } from 'react';

function useFilterTable(initialValue) {
  const [filtered, setFiltered] = useState([]);

  useEffect(() => {
    if (Array.isArray(initialValue)) {
      setFiltered(initialValue);
    }
  }, [initialValue]);

  const handleChange = (value) => {
    const listData = initialValue.map((item) => ({ ...item, key: item.id }));
    const filterTable = listData.filter((o) =>
      Object.keys(o).some((k) =>
        String(o[k]).toLowerCase().includes(value.toLowerCase())
      )
    );
    setFiltered(filterTable);
    if (value === '') {
      setFiltered(initialValue);
    } else {
      setFiltered(filterTable);
    }
  };

  return [filtered, handleChange];
}

export default useFilterTable;
