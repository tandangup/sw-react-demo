import { useState } from 'react';

const useDataTable = () => {
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [search, setSearch] = useState('');
  const [perPage, setPerPage] = useState(10);

  const handleChangePage = (data) => {
    setPage(data);
  };

  const handleChangePerpage = (data) => {
    setPerPage(data);
  };

  const handleSearch = (data) => {
    setSearch(data);
  };

  const handleChangeTotal = (data) => {
    setTotal(data);
  };

  return [
    page,
    total,
    search,
    perPage,
    handleSearch,
    handleChangePage,
    handleChangeTotal,
    handleChangePerpage,
  ];
};

export default useDataTable;
