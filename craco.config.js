const path = require('path');
const CompressionPlugin = require('compression-webpack-plugin');
const CracoLessPlugin = require('craco-less');
const purgecss = require('@fullhuman/postcss-purgecss');
const TerserPlugin = require('terser-webpack-plugin');
const cracoServiceWorkerConfig = require('./src/cracoServiceWorkerConfig');

// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const PurgecssPlugin = require('purgecss-webpack-plugin');
// const glob = require('glob-all');
const minify = (input, sourceMap, minimizerOptions, extractsComments) => {
  // The `minimizerOptions` option contains option from the `terserOptions` option
  // You can use `minimizerOptions.myCustomOption`

  // Custom logic for extract comments
  const { map, code } = require('uglify-module') // Or require('./path/to/uglify-module')
    .minify(input, {
      /* Your options for minification */
    });

  return { map, code, warnings: [], errors: [], extractedComments: [] };
};
minify.getMinimizerVersion = () => {
  let packageJson;

  try {
    // eslint-disable-next-line global-require, import/no-extraneous-dependencies
    packageJson = require('uglify-module/package.json');
  } catch (error) {
    // Ignore
  }
  return packageJson && packageJson.version;
};
module.exports = {
  webpack: {
    // webpack configs
    configure: {
      mode: 'development',
      // usedExport: true,
      // optimization: {
      //   minimize: true,
      //   minimizer: [
      //     new TerserPlugin({
      //       terserOptions: {
      //         myCustomOption: true,
      //       },
      //       minify,
      //     }),
      //   ],
      // },
    },
    alias: {
      '@images': path.join(path.resolve(__dirname, './src/assets/images')),
      '@logo': path.join(path.resolve(__dirname, './src/assets/images/logo')),
      '@icon': path.join(path.resolve(__dirname, './src/assets/images/icons')),
      '@css': path.join(path.resolve(__dirname, './src/assets/css')),
    },
    plugins: [
      new CompressionPlugin({
        filename: '[path][name].gz[query]',
        algorithm: 'gzip',
        test: /\.(js|css|html|svg)$/,
        threshold: 8192,
        minRatio: 0.8,
      }),
    ],
  },
  style: {
    // sass: {
    //   loaderOptions: {
    //     /* Any sass-loader configuration options: https://github.com/webpack-contrib/sass-loader. */
    // },
    //   loaderOptions: (sassLoaderOptions, { env, paths }) => {
    //     return sassLoaderOptions;
    //   },
    // },
    postcss: {
      plugins: [
        require('tailwindcss'),
        require('autoprefixer'),
        purgecss({
          content: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
          defaultExtractor: (content) =>
            content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || [],
          options: {
            safelist: ['w-*', 'h-*', 'bg-red-500', 'px-4'],
            blocklist: [/^debug-/],
            keyframes: true,
            fontFace: true,
          },
        }),
      ],
    },
  },
  plugins: [
    {
      plugin: {
        CracoLessPlugin,
        cracoServiceWorkerConfig,
      },
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              '@link-color': '#1DA57A',
              '@primary-color': '#1DA57A',
              '@border-radius-base': '2px',
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
