// eslint-disable-next-line no-undef
importScripts('https://unpkg.com/workbox-sw@6.4.2/build/workbox-sw.js');
// eslint-disable-next-line no-undef
importScripts(
  'https://unpkg.com/workbox-runtime-caching@2.0.3/build/importScripts/workbox-runtime-caching.prod.v2.0.3.js'
);
// eslint-disable-next-line no-undef
importScripts(
  'https://unpkg.com/workbox-routing@6.4.2/build/workbox-routing.prod.js'
);
// eslint-disable-next-line no-undef
importScripts(
  'https://unpkg.com/workbox-strategies@6.4.2/build/workbox-strategies.prod.js'
);
// eslint-disable-next-line no-undef
importScripts(
  'https://unpkg.com/workbox-expiration@6.4.2/build/workbox-expiration.prod.js'
);

// eslint-disable-next-line no-undef
const assetRoute = new workbox.routing.RegExpRoute({
  regExp: new RegExp('^http://localhost:3000/static/*'),
  // eslint-disable-next-line no-undef
  handler: new workbox.runtimeCaching.CacheFirst({
    plugins: [
      // eslint-disable-next-line no-undef
      new workbox.expiration.ExpirationPlugin({
        maxAgeSeconds: 24 * 60 * 60,
      }),
    ],
  }),
});
// eslint-disable-next-line no-undef
const router = new workbox.routing.Router();
//router.addFetchListener();
router.registerRoute({ routes: [assetRoute] });
router.setDefaultHandler({
  // eslint-disable-next-line no-undef
  handler: new workbox.runtimeCaching.CacheFirst(),
});
